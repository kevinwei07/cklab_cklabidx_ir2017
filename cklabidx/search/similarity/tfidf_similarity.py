#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import math

from cklabidx.search.similarity.base_similarity import Similarity


class TFIDFSimilarity(Similarity):
    """
    Document similarity scores using TF-IDF
    Extends Scores Class in base_scorer.py
    """

    def __init__(self, use_explain=False):
        super().__init__(use_explain)

        # Set notation for explain
        if self.use_explain:
            self.explain.notation = "doc_coord * query_norm * SUM[ (tf * idf^2 * term_boost * norm ) ]"

        # TF-IDF specific scoring elements
        self.doc_coords_dict = {}

        # coord(q,d) is a score factor based on how many of the query terms are found in
        # the specified document.
        #
        #     doc_coords = {
        #         "field1": {
        #             "doc1": "1",
        #             "doc2": "3",
        #             "doc3": "5", ...
        #         }, ...
        #     }

    def update_hit_doc_list(self, field, term, hit_doc_list):
        """
        Update hit document list, call once for each term.
        :param field:
        :param term:
        :param hit_doc_list:
        :return:
        """
        # self.scorer.add_term_hits(field, term, hit_list)
        # Update doc_coord for each term hits
        for doc in hit_doc_list:
            self._set_document_coord(field, doc)

    def _get_document_coord(self, field, doc):
        return self.doc_coords_dict[field][doc]

    def _set_document_coord(self, field, doc):
        """
        coord(q,d) is a score factor based on how many of the query terms are found in
        the specified document.

        :param field:
        :param doc:
        :return:
        """
        if field not in self.doc_coords_dict:
            self.doc_coords_dict[field] = {}

        if doc not in self.doc_coords_dict[field]:
            self.doc_coords_dict[field][doc] = 1
        else:
            self.doc_coords_dict[field][doc] += 1

    def _get_query_norm(self, field_name):
        """
        queryNorm(q) is a normalizing factor used to make scores between queries comparable.

        :param field_name:
        :return:
        """
        if not self.query_object:
            return 1

        sum_of_squared_weights = 0
        query_boost = 1
        term_boost = 1

        for term in self.query_object.query_dict[field_name]:
            idf = self.index_object.get_term_idf_lucene_default(field_name, term)
            sum_of_squared_weights += (idf * term_boost) ** 2

        query_sum_of_squared_weights = query_boost ** 2 * sum_of_squared_weights

        query_norm = 1 / query_sum_of_squared_weights ** 0.5

        return query_norm

    def get_scores(self, field, term_list, hit_doc_list):
        """
        Implements Lucene Class TFIDFSimilarity
        (Ref: https://lucene.apache.org/core/5_4_1/core/org/apache/lucene/search/similarities/TFIDFSimilarity.html)

        Practical Scoring Function
        score(q,d) = coord(q,d) · queryNorm(q) ·  ∑   ( tf(t in d) · idf(t)^2 · t.getBoost() · norm(t,d) )
                                                t in q
                     1.doc_coord
                                  2.query_norm
                                                 3. sum_tf_idf
                                                       (   4. tf     5. idf      6.term_boost   7. norm  )

        :param field:
        :param term_list:
        :param hit_doc_list:
        :return:
        """
        doc_scores_dict = {}

        # 2. queryNorm(q)
        query_norm = self._get_query_norm(field)

        for doc in hit_doc_list:

            # 1. coord(q, d)
            doc_coord = self._get_document_coord(field, doc)

            # 3. Iterate over all terms to get SUM of tf-idf
            sum_tf_idf_score = 0
            sum_tf_idf_explain_list = []    # Formula string

            for term in term_list:

                # 4. TF
                tf = math.sqrt(self.index_object.get_term_tf(doc, field, term))

                # 5. IDF
                idf = self.index_object.get_term_idf(field, term)

                # 6. termBoost
                # No implement, set default value to 1
                term_boost = 1

                # 7. norm(t,d)
                #
                #    norm(t,d) = lengthNorm ·         ∏                f.boost()
                #                             field f in d named as t
                #
                field_boost = 1  # No implement, set to 1
                length_norm = self.index_object.get_length_norm_of_doc(doc, field)
                norm = length_norm * field_boost

                # SUM of TF-IDF score
                sum_tf_idf_score += tf * idf ** 2 * term_boost * norm

                # Store scoring explain
                if self.use_explain:
                    sum_tf_idf_explain_list.append("({tf} * {idf}^2 * {tb} * {norm})".format(
                        tf=round(tf, 6),
                        idf=round(idf, 6),
                        tb=term_boost,
                        norm=round(norm, 6))
                    )

            # The final score!
            doc_score = doc_coord * query_norm * sum_tf_idf_score

            doc_scores_dict[doc] = doc_score

            # Store scoring explain
            if self.use_explain:
                sum_tf_idf_explain_str = " + ".join(sum_tf_idf_explain_list)
                explain = "{dc} * {qn} * [ {sum_tf_idf} ]".format(
                    dc=round(doc_coord, 6),
                    qn=round(query_norm, 6),
                    sum_tf_idf=sum_tf_idf_explain_str
                )

                self.explain.add_document(doc, explain, round(doc_score, 6))

        return doc_scores_dict
