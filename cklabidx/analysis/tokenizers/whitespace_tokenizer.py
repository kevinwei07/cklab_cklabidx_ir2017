#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import re

from cklabidx.analysis.tokenizers.base_tokenizer import Tokenizer


class WhitespaceTokenizer(Tokenizer):
    """
    WhitespaceTokenizer
    """

    def generate_tokens(self, text):
        """
        Tokenize text with whitespace

        :param text: Input text to be tokenize
        :return: A list of tokens
        """

        content = text.strip(' \t\n\r')
        print(content)
        tokens = re.split('\s+', content)
        print(tokens)
        #print(tokens[1])
        return tokens
