#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import re
import re,string
from cklabidx.analysis.tokenizers.base_tokenizer import Tokenizer


class CompoundTokenizer(Tokenizer):
    """
    CompoundTokenizer
    """

    def generate_tokens(self, text):
        """
        Break input text with whitespace and NGram.

        :param text: Input text to be tokenize
        :return: A list of tokens
        """

        # TODO: Add compound tokenizer
        tokens = []  # Dummy value
        content = text.strip(' \t\n\r')
        tokens = re.split('\s+', content)
        print(tokens)
        result = []
        
        for j in range (0,len(tokens)):
            word =''
            for i in tokens[j]:
                if i not in (string.ascii_letters+string.digits+string.punctuation):
                    if word != '':
                        result.append(word)
                        word = ''
                    result.append(i)
                else:
                    word+=i
            if word != '':
                result.append(word)
            print (result)        
        return result
