#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import re

from cklabidx.analysis.processors.base_processor import Processor


class PunctuationProcessor(Processor):
    """
    PunctuationProcessor

    # Regular expression to handle chinese characters from Jieba (https://github.com/fxsjy/jieba)
    re_eng = re.compile('[a-zA-Z0-9]', re.UNICODE)
    re_han_default = re.compile("([\u4E00-\u9FA5a-zA-Z0-9+#&\._]+)", re.UNICODE)
    re_skip_default = re.compile("(\r\n|\s)", re.UNICODE)
    re_han_cut_all = re.compile("([\u4E00-\u9FA5]+)", re.UNICODE)
    re_skip_cut_all = re.compile("[^a-zA-Z0-9+#\n]", re.UNICODE)
    """

    @staticmethod
    def process(input_text):
        """
        Remove all chinese punctuation
        :param input_text: Input
        :return: Processed text
        """
        return PunctuationProcessor._remove_en_punctuations(input_text)

    @staticmethod
    def _remove_en_punctuations(text):
        """
        Remove all non a-z, A-Z, 0-9 characters.
        :param text: input text
        :return: processed text
        """
        re_han_default = re.compile("[^\u4E00-\u9FA5a-zA-Z0-9]", re.UNICODE)
        
        #re_eng = re.compile('[^a-zA-Z0-9\s]', re.UNICODE)
        print(re.sub(re_han_default, '', text))
        return re.sub(re_han_default, '', text)
