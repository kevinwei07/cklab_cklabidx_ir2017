﻿#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#
# coding=gbk
import unittest
from cklabidx.analysis.tokenizers.whitespace_tokenizer import WhitespaceTokenizer


class WhitespaceTokenizerTest(unittest.TestCase):

    def test_whitespace_tokenizer(self):

        self.tokenizer = WhitespaceTokenizer()

        self.assertEqual(self.tokenizer.generate_tokens("a b c"), ["a", "b", "c"])
        self.assertEqual(self.tokenizer.generate_tokens("中文english 123"), ["中文english", "123"])
if __name__ == '__main__':
    unittest.main()
