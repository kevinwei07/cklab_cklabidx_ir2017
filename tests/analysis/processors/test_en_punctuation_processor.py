#! /usr/bin/env python3
#coding=utf-8
# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#
import unittest
from cklabidx.analysis.processors.en_punctuation_processor import PunctuationProcessor


class PunctuationProcessorTest(unittest.TestCase):

    def test_punctuation_processor(self):

        self.enpp = PunctuationProcessor()

        self.assertEqual(self.enpp.process("ABC"), "ABC")
        self.assertEqual(self.enpp.process("A,B,C;"), "ABC")
        self.assertEqual(self.enpp.process("A,B,C"), "ABC")
        self.assertEqual(self.enpp.process("{ABC}"), "ABC")
        self.assertEqual(self.enpp.process("[ABC]"), "ABC")
        self.assertEqual(self.enpp.process("!@#$%^&*()[]{}|"), "")
        self.assertEqual(self.enpp.process("測，"), "測")


if __name__ == '__main__':
    unittest.main()
