#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import unittest
from cklabidx.analysis.filters.upper_case_filter import UpperCaseFilter


class UpperCaseFilterTest(unittest.TestCase):

    def test_uppercase_filter(self):

        self.filter = UpperCaseFilter()

        self.assertEqual(self.filter.filter(["Aa", "Bb", "cc"]), ["AA", "BB", "CC"])
        self.assertEqual(self.filter.filter(["AbCdEf", "012345"]), ["ABCDEF", "012345"])


if __name__ == '__main__':
    unittest.main()
